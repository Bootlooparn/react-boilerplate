import webpack from 'webpack'
import HtmlwebpackPlugin from 'html-webpack-plugin'
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin'
import CompressionPlugin from 'compression-webpack-plugin'
import BundleAnalyzerPlugin from 'webpack-bundle-analyzer'

const bundleAnalyzer = new BundleAnalyzerPlugin.BundleAnalyzerPlugin()

const configuration: webpack.Configuration = {
    mode: 'production',
    devtool:false,
    entry: './src/app.tsx',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/dist'
    },
    module: {
        rules: [{
            test: /\.tsx$/,
            use: {
                "loader": "ts-loader",
                "options": {
                    "transpileOnly": true
                }
            },
            exclude: /node_modules/
        },
        {
            test: /\.(css|scss)$/,
            use: ['style-loader', 'css-loader', 'sass-loader']
        }]
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".scss", "sass"]
    },
    plugins: [
        bundleAnalyzer,
        new HtmlwebpackPlugin({
            inject:"head",
            template: './template/index.html',
            title: 'boilerplate'
        }),
        new CompressionPlugin()
    ],
    optimization: {
        minimizer: [ new CssMinimizerPlugin() ]
    }
}

export default configuration